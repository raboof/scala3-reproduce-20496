object Actor:
  type Receive = PartialFunction[Any, Unit]
trait Actor: // interface
  type Receive = Actor.Receive
  implicit val context: String = ""
  final def sender(): String = ""
  def receive(): Unit
  def preStart(): Unit = ()

trait Timers extends Actor: // interface extends Actor
  val timersVal = "foo"
  private val _timers = ???
  final def timers = _timers

abstract class ShardCoordinator extends Actor with Timers: // abstract class implements Actor,Timers
  val log = ""
  def foo = ""
trait PersistentActor extends Eventsourced with PersistenceIdentity:
  def receive = ???
  def defer[A](event: A)(handler: A => Unit): Unit = ()
trait Eventsourced extends Snapshotter with PersistenceStash with PersistenceIdentity with PersistenceRecovery:
  lazy val snapshotStore = ???
  override def snapshotterId: String = ???
trait Snapshotter extends Actor:
  def snapshotStore: String
  def snapshotterId: String
  val ssv = 8
trait PersistenceRecovery
trait PersistenceIdentity
trait PersistenceStash extends Stash with StashFactory
trait RequiresMessageQueue[T]
trait Stash extends UnrestrictedStash with RequiresMessageQueue[String]
trait UnrestrictedStash extends Actor with StashSupport
trait StashSupport:
  private var theStash = Vector.empty
trait StashFactory { this: Actor =>
}

class PersistentShardCoordinator extends ShardCoordinator with PersistentActor:
  super.preStart()
  super.timers
